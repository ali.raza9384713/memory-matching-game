const primaryColor = "#5acba1";
const successColor = "#198754";
const warningColor = "#ffc107";
const errorColor = "#dc3545";

const antdTheme = {
  "@primary-color": primaryColor,
  "@success-color": successColor,
  "@warning-color": warningColor,
  "@error-color": errorColor,
};

const customTheme = {
  ...antdTheme,
  // Add your custom theme variables here
};

export default customTheme;
