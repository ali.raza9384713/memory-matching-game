import React from "react";

import { CardProps } from "./Card.interface";
import { StyledCard } from "./Card.styled";

export const Card: React.FC<CardProps> = ({ item, onCardClicked }) => {
  return (
    <StyledCard cardStatus={item.status} onClick={() => onCardClicked(item)}>
      <img src={item.icon} alt="Flag Image" />
    </StyledCard>
  );
};
