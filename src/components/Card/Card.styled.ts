import styled from "styled-components";
import { CardStatus } from "./../../hooks/useMemoryBoard/useMemoryBoard.interface";
import { StyledCardProps } from "./Card.interface";

export const StyledCard = styled.div<StyledCardProps>`
  padding: 10px;
  border-radius: 15px;
  text-align: center;

  background-color: ${({ theme, cardStatus }) =>
    cardStatus === CardStatus.paired
      ? theme["@success-color"]
      : theme["@error-color"]};

  cursor: ${({ cardStatus }) =>
    cardStatus === CardStatus.paired || cardStatus === CardStatus.selected
      ? "not-allowed"
      : "pointer"};

  background-color: ${({ cardStatus }) =>
    cardStatus === CardStatus.default && `lightbrown`};

  img {
    border-radius: 15px;
    width: 80px;
    height: 80px;
    transition: 0.15s ease-in-out;
    filter: ${({ cardStatus }) =>
      cardStatus === CardStatus.default
        ? "blur(50px)"
        : cardStatus === CardStatus.selected
        ? "blur(0px)"
        : null};
    -webkit-user-drag: none;
    -khtml-user-drag: none;
    -moz-user-drag: none;
    -o-user-drag: none;
  }
`;
