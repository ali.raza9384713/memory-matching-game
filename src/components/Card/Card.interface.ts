import {
  CardStatus,
  BoardStateItem,
} from "./../../hooks/useMemoryBoard/useMemoryBoard.interface";

export interface CardProps {
  item: BoardStateItem;
  onCardClicked: (incomingItem: BoardStateItem) => void;
}

export interface StyledCardProps {
  cardStatus: CardStatus;
}
