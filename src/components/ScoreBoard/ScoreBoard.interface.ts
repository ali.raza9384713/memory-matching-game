export interface ScoreBoardProps {
    getMemoizedPairs: string
    tries: string | number
}
