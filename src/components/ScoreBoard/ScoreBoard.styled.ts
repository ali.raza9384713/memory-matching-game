import styled from "styled-components";

export const StyledScoreBoard = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 30px;
  font-size: 20px;
`;
