import React from "react";

import { ScoreBoardProps } from "./ScoreBoard.interface";
import { StyledScoreBoard } from "./ScoreBoard.styled";

export const ScoreBoard: React.FC<ScoreBoardProps> = ({
  getMemoizedPairs,
  tries,
}) => {
  return (
    <StyledScoreBoard>
      <span>Pairs: {getMemoizedPairs}</span>
      <span>Tries: {tries}</span>
    </StyledScoreBoard>
  );
};
