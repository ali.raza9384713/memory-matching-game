import React from "react";

import { ModalProps } from "./Modal.interface";
import {
  ModalWrapper,
  ModalContent,
  ModalTitle,
  ModalBody,
  ModalButton,
} from "./Modal.styled";

export const Modal: React.FC<ModalProps> = ({ isOpened, resetBoardState }) => {
  if (!isOpened) return <></>;
  return (
    <ModalWrapper>
      <ModalContent>
        <ModalTitle>🎉 Congrats 🎉</ModalTitle>
        <ModalBody>Game Completed</ModalBody>
        <ModalButton onClick={resetBoardState}>Reset Game</ModalButton>
      </ModalContent>
    </ModalWrapper>
  );
};
