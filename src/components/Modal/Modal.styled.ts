import styled from "styled-components";

export const ModalWrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 999;
`;

export const ModalContent = styled.div`
  background-color: #fff;
  padding: 20px;
  border-radius: 10px;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
  max-width: 250px;
  width: 100%;
  text-align: center;
  position: relative;
  font-size: 16px;
`;

export const ModalButton = styled.button`
  background-color: #3f51b5;
  color: #fff;
  border: none;
  border-radius: 5px;
  padding: 10px 20px;
  cursor: pointer;
  font-size: 16px;
  transition: all 0.2s ease-in-out;

  &:hover {
    background-color: #5d6cb2;
  }
`;

export const ModalCloseButton = styled.button`
  position: absolute;
  top: 5px;
  right: 5px;
  font-size: 18px;
  font-weight: bold;
  border: none;
  background-color: transparent;
  cursor: pointer;
`;

export const ModalTitle = styled.h2`
  font-size: 24px;
  margin-bottom: 20px;
`;

export const ModalBody = styled.div`
  font-size: 18px;
  margin-bottom: 20px;
`;
