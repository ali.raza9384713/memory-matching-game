export interface ModalProps {
  isOpened: boolean;
  onClose?: () => {};
  resetBoardState: () => void;
}
