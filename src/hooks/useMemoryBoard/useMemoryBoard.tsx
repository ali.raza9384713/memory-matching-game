import React from "react";

import shuffleArray from "../../utils/shuffleArray";
import {
  useMemoryBoardProps,
  CardStatus,
  CardValues,
  BoardStateItem,
} from "./useMemoryBoard.interface";

import pakImg from "./../../assets/imgs/pakistan.png";
import turkeyImg from "./../../assets/imgs/turkey.png";
import ukImg from "./../../assets/imgs/uk.png";
import usaImg from "./../../assets/imgs/usa.png";
import koreaImg from "./../../assets/imgs/korea.png";
import indiaImg from "./../../assets/imgs/india.png";

const initBoardState: BoardStateItem[] = [
  { key: 0, value: CardValues.Pak, icon: pakImg, status: CardStatus.default },
  { key: 1, value: CardValues.Pak, icon: pakImg, status: CardStatus.default },
  { key: 2, value: CardValues.Ind, icon: indiaImg, status: CardStatus.default },
  { key: 3, value: CardValues.Ind, icon: indiaImg, status: CardStatus.default },
  { key: 4, value: CardValues.UK, icon: ukImg, status: CardStatus.default },
  { key: 5, value: CardValues.UK, icon: ukImg, status: CardStatus.default },
  { key: 6, value: CardValues.USA, icon: usaImg, status: CardStatus.default },
  { key: 7, value: CardValues.USA, icon: usaImg, status: CardStatus.default },
  { key: 8, value: CardValues.Kor, icon: koreaImg, status: CardStatus.default },
  { key: 9, value: CardValues.Kor, icon: koreaImg, status: CardStatus.default },
  {
    key: 10,
    value: CardValues.Tur,
    icon: turkeyImg,
    status: CardStatus.default,
  },
  {
    key: 11,
    value: CardValues.Tur,
    icon: turkeyImg,
    status: CardStatus.default,
  },
];

export const useMemoryBoard = () => {
  const [boardState, setBoardState] = React.useState<BoardStateItem[]>(
    shuffleArray(structuredClone(initBoardState))
  );
  const [tries, setTries] = React.useState(0);
  const [isEnded, setIsEnded] = React.useState(false);

  const onCardClicked = (incomingItem: BoardStateItem) => {
    if (incomingItem.status === CardStatus.paired) return;
    const prevSelectedItem = boardState.find(
      (item) => item.status === CardStatus.selected
    );
    if (prevSelectedItem?.key === incomingItem.key) return;
    let newBoardState = boardState.map((item) => {
      if (item.key === incomingItem.key) {
        if (prevSelectedItem) {
          if (prevSelectedItem.value === item.value) {
            prevSelectedItem.status = CardStatus.paired;
            item.status = CardStatus.paired;
            return item;
          } else {
            setTries(tries + 1);
            prevSelectedItem.status = CardStatus.default;
          }
        }
        if (item.status === CardStatus.default) {
          item.status = CardStatus.selected;
          return item;
        }
      }

      return item;
    });

    if (prevSelectedItem) {
      newBoardState = newBoardState.map((item) => {
        if (item.key === prevSelectedItem.key) return prevSelectedItem;
        return item;
      });
    }

    const isGameEnded = newBoardState.every(
      (item) => item.status === CardStatus.paired
    );
    setIsEnded(isGameEnded);
    setBoardState(newBoardState);
  };

  const resetBoardState = () => {
    setBoardState(shuffleArray(structuredClone(initBoardState)));
    setTries(0);
    setIsEnded(false);
  };

  const getMemoizedPairs = React.useMemo(() => {
    const pairedSets = boardState.filter(
      (item) => item.status === CardStatus.paired
    ).length;
    const totalCards = boardState.length;
    return `${pairedSets / 2} / ${totalCards / 2}`;
  }, [boardState]);

  return {
    boardState,
    tries,
    onCardClicked,
    getMemoizedPairs,
    resetBoardState,
    isEnded,
  };
};
