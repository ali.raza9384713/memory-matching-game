export interface useMemoryBoardProps {}

export enum CardStatus {
  default,
  selected,
  paired,
}

export enum CardValues {
  Pak,
  Ind,
  UK,
  USA,
  Kor,
  Can,
  Ira,
  Tur,
}

export interface BoardStateItem {
  key: string | number;
  value: CardValues;
  icon: any;
  status: CardStatus;
}
