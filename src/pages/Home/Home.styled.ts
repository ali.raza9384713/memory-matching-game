import styled from "styled-components";
import { CardStatus } from "./../../hooks/useMemoryBoard/useMemoryBoard.interface";

export const StyledHome = styled.div`
  background: #ffffff url(https://res.cloudinary.com/dpcm48yi6/image/upload/v1680156260/main-bg-min_ty07gq.jpg) no-repeat center center / cover
    padding-box border-box fixed;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const StyledBoard = styled.div`
  margin: auto;
  border-radius: 20px;
  position: relative;
  overflow: hidden;
  padding: 10px;

  @media (min-width: 767px) {
    max-width: 400px;
    padding: 50px;
  }

  &::before {
    content: "";
    position: absolute;
    top: -50px;
    left: -50px;
    right: -50px;
    bottom: -50px;
    background: inherit;
    backdrop-filter: brightness(50%) blur(10px);
    opacity: 0.5;
    z-index: -1;
    border-radius: inherit;
  }
  .grid-container {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    grid-template-rows: repeat(4, 1fr);
    gap: 12px;
  }
`;
