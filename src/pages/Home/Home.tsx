import React from "react";

import { HomeProps } from "./Home.interface";
import { StyledHome, StyledBoard } from "./Home.styled";

import { useMemoryBoard } from "../../hooks/useMemoryBoard";
import { Card } from "../../components/Card";
import { ScoreBoard } from "../../components/ScoreBoard/ScoreBoard";
import { Modal } from "../../components/Modal";

export const Home: React.FC<HomeProps> = () => {
  const {
    boardState,
    getMemoizedPairs,
    onCardClicked,
    resetBoardState,
    tries,
    isEnded,
  } = useMemoryBoard();

  return (
    <StyledHome>
      <div className="container">
        <Modal isOpened={isEnded} resetBoardState={resetBoardState} />
        <StyledBoard>
          <ScoreBoard getMemoizedPairs={getMemoizedPairs} tries={tries} />
          <div className="grid-container">
            {boardState.map((item) => (
              <Card item={item} key={item.key} onCardClicked={onCardClicked} />
            ))}
          </div>
        </StyledBoard>
      </div>
    </StyledHome>
  );
};
